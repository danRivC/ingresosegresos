import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { IngresoEgreso } from 'src/app/models/ingreso-egreso.model';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { IngresoEgresoService } from 'src/app/services/ingreso-egreso.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit, OnDestroy {
  ingresoEgreso:IngresoEgreso[]=[];
  ingresoSubs:Subscription;

  constructor(
    private store:Store<AppState>,
    private ingresoEgresoService:IngresoEgresoService) { }

  ngOnInit(): void {
    this.ingresoSubs=this.store.select('ingresoEgreso')
    .pipe(filter(ingreso => ingreso.items.length > 0))
    .subscribe(({items}) => {
      this.ingresoEgreso = items;
    })
  }
  ngOnDestroy(){
    this.ingresoSubs.unsubscribe();
  }
  borrar(uidItem:string){
    this.ingresoEgresoService.borrarIngresoEgreso(uidItem)
    .then(() => Swal.fire('Borrado', 'Item Borrado', 'success'))
    .catch(err=> Swal.fire('Error', err.message, 'error'));
  }

}
