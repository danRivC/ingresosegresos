import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { IngresoEgresoService } from 'src/app/services/ingreso-egreso.service';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
@Component({
  selector: 'app-estadistica',
  templateUrl: './estadistica.component.html',
  styleUrls: ['./estadistica.component.css']
})
export class EstadisticaComponent implements OnInit, OnDestroy {
  datosSubs:Subscription;
  ingresos = 0;
  egresos = 0;
  totalIngresos = 0;
  totalEgresos = 0;
  public doughnutChartLabels: Label[] = ['Ingresos', 'Egresos'];
  public doughnutChartData: MultiDataSet = [[]];
  public doughnutChartType: ChartType = 'doughnut';
  constructor(private store:Store<AppState>, private ingresoEgresoService:IngresoEgresoService) { }

  ngOnInit(): void {
    this.datosSubs = this.store.select('ingresoEgreso')
    .pipe(
      filter( datos =>datos.items.length > 0)
      )
      .subscribe(({items}) => {
        this.ingresos = 0;
        this.egresos = 0;
        this.totalIngresos = 0;
        this.totalEgresos = 0;
        items.forEach(element => {
          if(element.tipo==='ingreso'){
            this.totalIngresos +=element.monto;
            this.ingresos++
          }else{
            this.totalEgresos +=element.monto;
            this.egresos++
          }
        });
        this.doughnutChartData = [[this.totalIngresos, this.totalEgresos]];

      })
    

  }
  ngOnDestroy(){
    this.datosSubs.unsubscribe()
  }

}
