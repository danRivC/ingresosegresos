import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IngresoEgreso } from '../models/ingreso-egreso.model';
import { IngresoEgresoService } from '../services/ingreso-egreso.service';
import Swal from 'sweetalert2';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import * as uiActions from '../shared/ui.actions';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ingreso-egreso',
  templateUrl: './ingreso-egreso.component.html',
  styleUrls: ['./ingreso-egreso.component.css'],
})
export class IngresoEgresoComponent implements OnInit, OnDestroy {
  ingresoForm: FormGroup;
  tipo = 'ingreso';
  cargando = false;
  suscription : Subscription;
  constructor(
    private formBuilder: FormBuilder,
    private ingresoEgresoService: IngresoEgresoService,
    private store: Store<AppState>
    ) {
    this.buildForm();
  }

  buildForm() {
    this.ingresoForm = this.formBuilder.group({
      txt_descripcion: ['', Validators.required],
      txt_monto: [0, Validators.required],
    });
  }
  ngOnInit(): void {
    this.suscription = this.store.select('ui').subscribe(({isLoading}) => this.cargando = isLoading);
  }
  ngOnDestroy(){
    this.suscription.unsubscribe();

  }

  guardar(){

    if(this.ingresoForm.valid){
      this.store.dispatch(uiActions.isLoading());
      const {txt_descripcion, txt_monto} = this.ingresoForm.value;
      const ingresoEgreso = new IngresoEgreso(txt_descripcion, txt_monto, this.tipo);
      this.ingresoEgresoService.crearIngresoEgreso(ingresoEgreso)
      .then(() => {
        this.store.dispatch(uiActions.stopLoading());
        this.ingresoForm.reset();
        Swal.fire('Registro creado', txt_descripcion, 'success');
      })
      .catch( err => {
        this.store.dispatch(uiActions.stopLoading());
        Swal.fire('Error', err.message, 'error');
      });
    }

  }
}
