import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {
  usuario = '';
  suscription:Subscription
  constructor(private authService: AuthService, private router: Router, private store:Store<AppState>) { }

  ngOnInit(): void {
    this.suscription = this.store.select('auth').pipe(filter(usuario => usuario.user !== null)).subscribe(({user})=>{
      this.usuario = user.nombre;
    })
  }
  ngOnDestroy(){
    this.suscription.unsubscribe();
  }

  logout(){
    Swal.fire({
      title: 'Espere por favor',
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    this.authService.logout()
    .then( () => {
      Swal.close();
      this.router.navigate(['/login']);
    })
    .catch( (err) => {
      console.log(err);
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: err.message
      });
    })
  }

}
