import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {
  usuario = '';
  suscription:Subscription
  constructor(private store:Store<AppState>) { }

  ngOnInit(): void {
    this.suscription = this.store.select('auth').pipe(filter(usuario => usuario.user !== null)).subscribe(({user})=>{
      this.usuario = user.nombre;
    })
  }
  ngOnDestroy(){
    this.suscription.unsubscribe();
  }

}
