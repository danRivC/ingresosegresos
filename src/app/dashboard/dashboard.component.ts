import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { IngresoEgresoService } from '../services/ingreso-egreso.service';
import * as ingresoEgresoActions from '../ingreso-egreso/ingreso-egreso.actions';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  userSubs:Subscription;
  ingresoEgresosSubs:Subscription;
  constructor(private store:Store<AppState>, private ingresoEgresoService:IngresoEgresoService) { }

  ngOnInit(): void {
     this.userSubs = this.store.select('auth')
    .pipe(filter(usuario => usuario.user !== null))
    .subscribe(({user}) => {
      this.ingresoEgresosSubs= this.ingresoEgresoService.initIngresoEgresoListener(user.uid)
      .subscribe(
        ingresoEgreso=>{
          this.store.dispatch(ingresoEgresoActions.setItems({items: ingresoEgreso}));

        }
      );
    })
  }
  ngOnDestroy(){
    this.userSubs.unsubscribe();
    this.ingresoEgresosSubs.unsubscribe();
  }

}
