import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import * as ui from 'src/app/shared/ui.actions';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  cargando: boolean = false;
  uiSubscription:Subscription;

  constructor(
    private formBuilder: FormBuilder, 
    private authService: AuthService,
    private router: Router,
    private store: Store<AppState>) { 
    this.buildForm();
  }
  buildForm(){
    this.loginForm = this.formBuilder.group({
      txt_email: ['', [Validators.required, Validators.email]],
      txt_password: ['', Validators.required]
    });
  }
  ngOnInit(): void {
    this.uiSubscription = this.store.select('ui').subscribe(ui => {
      this.cargando = ui.isLoading;
      console.log('Cargando');
    });
  }
  ngOnDestroy(): void {
    this.uiSubscription.unsubscribe();
  }

  loginUser(){

    if (this.loginForm.valid){
      this.store.dispatch( ui.isLoading() );
      this.authService.loginUsuario(this.loginForm.value.txt_email, this.loginForm.value.txt_password)
      .then((credenciales) => {
        console.log(credenciales);
        this.store.dispatch(ui.stopLoading());
        this.router.navigate(['/']);
      }).catch( (err) => {
        this.store.dispatch(ui.stopLoading());
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: err.message
        });
      })  ;
    }else{

      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'El formulario no es válido'
      });
    }
  }

}
