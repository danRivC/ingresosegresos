import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { Subscription } from 'rxjs';
import * as ui from 'src/app/shared/ui.actions';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  registerForm: FormGroup;
  uiSuscription: Subscription;
  cargando: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private store:Store<AppState>
  ) {
    this.buildForm();
  }

  buildForm() {
    this.registerForm = this.formBuilder.group({
      password: ['', Validators.required],
      correo: ['', [Validators.required, Validators.email]],
      name: ['', Validators.required],
    });
  }
  get correo(){return this.registerForm.get('correo')};
  get password(){return this.registerForm.get('password')};
  get name(){return this.registerForm.get('correo')};

  ngOnInit(): void {
    this.uiSuscription = this.store.select('ui').subscribe(
      ui=>{
        this.cargando = ui.isLoading;
        console.log('cargando desde registro');
      }
    );
  }

  ngOnDestroy(): void{
    this.uiSuscription.unsubscribe();
  }

  crearUsuario() {
    if (this.registerForm.valid) {
      this.store.dispatch(ui.isLoading());
      const { name, correo, password } = this.registerForm.value;
      this.authService
        .crearUsuario(name, correo, password)
        .then((credenciales) => {
          console.log(credenciales);
          this.store.dispatch( ui.stopLoading());
          this.router.navigate(['/']);
        })
        .catch((err) =>  Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: err.message
        }));
    }else{
      this.store.dispatch( ui.stopLoading());
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'El formulario no es válido'
      });
    }
  }
}
