import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { Usuario } from '../models/usuario.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import * as autActions from '../auth/auth.actions';
import { Subscription } from 'rxjs';
import * as ingresoEgresoActions from '../ingreso-egreso/ingreso-egreso.actions'
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userSuscription:Subscription;
  private _user: Usuario;
  constructor(
    private firebaseAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private store: Store<AppState>) { }

  initUserListener(){
    this.firebaseAuth.authState.subscribe(firebaseUser => {
      if(firebaseUser){
        this.userSuscription = this.firestore.doc(`${firebaseUser.uid}/usuario`).valueChanges()
        .subscribe( (firestoreUser:any) => {
          const user = Usuario.firebaseUserToUser(firestoreUser);
          this._user = user;
          this.store.dispatch( autActions.setUser({user}) );
        });
      }else{
        this._user = null;
        this.userSuscription.unsubscribe();
        this.store.dispatch(autActions.unSetUser());
        this.store.dispatch( ingresoEgresoActions.unsetItems() );
      }
    });
  }

  get user(){
    return this._user;
  }

  crearUsuario(nombre: string, email: string, password: string){
    return this.firebaseAuth.createUserWithEmailAndPassword(email, password)
    .then( ({ user }) => {
      const newUser = new Usuario(user.uid, nombre, user.email);
      return this.firestore.doc(`${user.uid}/usuario`).set( {...newUser} );
    });

  }

  loginUsuario(email: string, password: string){
    return this.firebaseAuth.signInWithEmailAndPassword(email, password);
  }
  logout(){
    return this.firebaseAuth.signOut();
  }
  isAuth(){
    return this.firebaseAuth.authState.pipe(
      map(fbUser => fbUser != null)
    );
  }

}
