import { Injectable } from '@angular/core';
import 'firebase/firestore';
import {AngularFirestore} from '@angular/fire/firestore';
import { IngresoEgreso } from '../models/ingreso-egreso.model';
import { firestore } from 'firebase';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class IngresoEgresoService {

  constructor(
    private fireStore: AngularFirestore,
    private authService: AuthService
  ) { }

  crearIngresoEgreso(ingresoEgreso:IngresoEgreso){
    const uid = this.authService.user.uid;
    delete ingresoEgreso.uid;
    return this.fireStore.doc(`${uid}/ingreso-egreso`)
    .collection('items')
    .add({...ingresoEgreso});
  }

  initIngresoEgresoListener(uid:string){
    return this.fireStore.collection(`${uid}/ingreso-egreso/items`)
    .snapshotChanges()
    .pipe(
      map(snapShot=>{
        return snapShot.map( doc => {
          return{
            uid: doc.payload.doc.id,
            ...doc.payload.doc.data() as any
          }
        })
      })
    )
  }

  borrarIngresoEgreso(uidItem:string){
    const uid = this.authService.user.uid;
    return this.fireStore.doc(`${uid}/ingreso-egreso/items/${uidItem}`).delete();
  }
}
